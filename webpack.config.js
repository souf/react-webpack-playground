const webpack = require('webpack');
const path = require('path');

module.exports = {
    mode: 'development',
    entry: './src/index.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.(jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
        ]
    },
    devServer: {
        host: 'localhost',
        port: 3035,
        hot: false,
        devMiddleware: {
            index: true,
            serverSideRender: false,
            writeToDisk: true
        }
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
};